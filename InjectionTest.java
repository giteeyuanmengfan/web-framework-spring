package com.smart.Injection;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class InjectionTest {
    public static void main(String[] args) {
        //加载xml配置文件路径
        String xmlpath = "com/smart/Injection/beans3.xml";
        //实例化spring IOC容器
        ApplicationContext ctx = new ClassPathXmlApplicationContext(xmlpath);
        //获取bean
        LiuDeHua LiuDeHua_1 = (LiuDeHua) ctx.getBean("LiuDeHua_1");
        LiuDeHua LiuDeHua_2 = (LiuDeHua) ctx.getBean("LiuDeHua_2");
        //打印一下，看内容
        System.out.println(LiuDeHua_1);
        System.out.println(LiuDeHua_2);
    }
}
