package com.smart.Injection;
import java.util.List;
public class LiuDeHua {
    private String name;  //姓名
    private int age;    //年龄
    private String sex;   //性别
    private List<String> list;
    //有参构造方法
    public LiuDeHua(String name,int age,String sex,List<String> list){
        super();
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.list = list;
    }
    //无参构造方法
    public LiuDeHua(){
        super();
    }

    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public int getAge(){
        return age;
    }
    public void setAge(int age){
        this.age = age;
    }
    public String getSex(){
        return sex;
    }
    public void setSex(String sex){
        this.sex = sex;
    }
    public List<String> getList(){
        return list;
    }
    public void setList(List<String> list){
        this.list = list;
    }
    @Override
    public String toString(){
        return "com.smart.Injection.LiuDeHua [name=" + name + ",age="+ age +",sex="+ sex +",list="+ list +"]";
    }

}
