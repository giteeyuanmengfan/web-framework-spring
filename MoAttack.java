package com.smart.Injection;

import com.smart.ioc.GeLi;

public class MoAttack {

	private GeLi geli;

	//——————————1、构造函数注入——————————
	//①注入革离的具体饰演者
//	public MoAttack(GeLi geli){
//		this.geli = geli;
//	}
//	public void cityGateAsk() {
//		geli.responseAsk("墨者革离");
//	}
//
//	public void injectGeli(GeLi geli) {
//	}

	//——————————2、属性注入——————————
	public void setGeli(GeLi geli){
		this.geli=geli;
	}
	public void cityGateAsk() {
		geli.responseAsk("墨者革离");
	}

	public void injectGeli(GeLi geLi) {
	}

	//——————————3、接口注入——————————
//public class MoAttack implements ActorArrangable {
//	private GeLi geli;
//	public void injectGeli(GeLi geli) {
//		this.geli = geli;
//	}
//	public void cityGateAsk() {
//		geli.responseAsk("墨者革离");
//	}
}