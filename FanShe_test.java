package com.smart.Injection;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

//下面通过反射对name进行set值
public class FanShe_test {
    public static void main(String[] args) throws SecurityException, NoSuchMethodException {
        try {
            Class personClazz=Class.forName("com.smart.Injection.LiuDeHua");
            try {
                Object bean=personClazz.newInstance();
                Method method1=  personClazz.getMethod("setName","LiuDeHua".getClass());
           //     Method method2=  personClazz.getMethod("setAge", "18".getClass());
                Method method3=  personClazz.getMethod("setSex", "M".getClass());
          //      Method method4=  personClazz.getMethod("setList","墨者革离！".getClass());
                //调用bean实例的setName方法
                try {
                    method1.invoke(bean,"LiuDeHua");
              //      method2.invoke(bean,"18");
                    method3.invoke(bean,"M");
             //       method4.invoke(bean,"墨者革离！");
                    LiuDeHua p=(LiuDeHua)bean;
                    //测试是否已经将值set进去
                    System.out.println(p.getName());
                    System.out.println(p.getSex());
                } catch (IllegalArgumentException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } catch (InstantiationException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}
